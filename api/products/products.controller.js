const _ = require("lodash");
const Controller = require("../../common/controller").Controller;
const Sequelize = require("sequelize");
const Serializer = require("sequelize-json-serializer");
const HttpStatus = require("http-status-codes");
const Models = require("../../models");

const Schema = {

    fields: {
        'product_name': 'name',
        'quantity': ''
    }

};

Serializer.defineSchema(Models.product_quantity, Schema);

class ProductsController extends Controller {

    constructor(){
        super('Products',Models.product_quantity);
    }

    getName(req,res){

        return this.model.findOne({
            where:{
                product_name: req.params.name
            }
        }).then(model => {

            if(!model)
                return this.notFound(res,"Product not found");

            return this.onSerialize(req,model).then(result => {
                return this.data(res,result);
            });

        });

    }

    updateSingle(req,res){

        return this._updateSingle(req,res,undefined,undefined,[
            'quantity'
        ]);

    }
}

module.exports = ProductsController;