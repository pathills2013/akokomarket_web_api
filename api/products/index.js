const router = require("express").Router();
const _mgr = require("../../common/controller.manager");
const Controller = _mgr.createInstance( require("./products.controller") );
const Joi = require("joi");
const ev = require("express-validation");

router.get('/', Controller.method('getAll'));

router.get('/:name', ev( {  params: { name: Joi.string().required() }  }  ) ,Controller.method('getName'));

router.put('/:id', ev( {  params: { id: Joi.number().required() }  }  )  ,Controller.method('updateSingle'));

module.exports = router;