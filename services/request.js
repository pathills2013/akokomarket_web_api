const _ = require("lodash");
const notification = require("../notifications");
const Channels = require("../notifications/channels");
const Events = require("../notifications/events");
const Serializer = require("sequelize-json-serializer");
const Models = require("../models");

/**
 * Payment Successful
 */
exports.onPaymentComplete = function (request , transaction ) {

    notification.broadcast( Channels.Payment , Events.payment.paid , {
        amount: transaction.actual_amount,
        request: Serializer.serialize(request,Models.market_request,{
            tags: [ "notification" , "default" ],
            include: { all: true }
        }),

    });

};

/**
 * Payment Failed
 */
exports.onPaymentFail = function () {

    //  Log payment failure

};