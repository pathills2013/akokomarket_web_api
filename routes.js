const _ = require("lodash");
const router = require("express").Router();


module.exports = function (app) {

    //  Market Requests
    app.use("/api/market_requests", require("./api/market_requests") );

    app.use("/api/customers", require("./api/customers"));

    //  Depot
    app.use("/api/depot", require("./api/depot"));

    //  Sell Survey
    app.use("/api/sell_survey", require("./api/market_sell_survey"));

    //  Admin
    app.use("/api/admin", require("./api/admin"));

    //  Transactions
    app.use("/api/transactions", require("./api/transactions"));

    //  Products
    app.use("/api/products", require("./api/products"));

    //  Payment
    app.use("/payment", require("./api/payment") );


};